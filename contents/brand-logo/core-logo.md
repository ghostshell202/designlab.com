---
name: Core logo
---

The core [logo](https://about.gitlab.com/press/press-kit/) consists of the full-color logomark and wordmark in horizontal format. The core logo is the standard logo orientation to be used across all marketing materials, when the application or surrounding elements are ideal for the width of its proportions.

## Wordmark

The name GitLab is inspired by the name of the Git open source version control system that GitLab uses, and the collaboration, experimentation, and innovation that occurs when teams iterate in a laboratory.

The wordmark is a modified version of [Inter Semi Bold](https://fonts.google.com/specimen/Inter). The wordmark can’t be used independently and should always be presented alongside the logomark.

<figure class="figure" role="figure" aria-label="GitLab logo anatomy">
  <img class="figure-img p-a-5 img-50" src="/img/brand/gitlab-logo-anatomy.svg" alt="Anatomy of the GitLab logo" role="img" />
  <figcaption class="figure-caption">GitLab logo anatomy</figcaption>
</figure>

## Clear space

Clear space is the area, equal to the x-height, surrounding the entire logo; the x-height is measured by the height of the letter a from the wordmark.

This area should be kept free of any visual elements, including text, graphics, borders, patterns, and other logos. Ensuring proper clear space between the logo and surrounding elements preserves the visual integrity of our brand.

<figure class="figure" role="figure" aria-label="with clear space equal to the width of the lowercase 'a' from the wordmark">
  <img class="figure-img p-a-5 img-50" src="/img/brand/core-logo-clearspace.svg" alt="GitLab logo with clear space" role="img" />
  <figcaption class="figure-caption">GitLab logo with clear space equal to the width of the lowercase "a" from the wordmark</figcaption>
</figure>

## Scalability

When scaling either the logo or logomark, it should take up no more than 20% of the height of the frame of the asset. The percentage scales shown are based on the page height of 1080px.

The minimum size is 20px for digital and 0.4” (11mm) for print.

<figure class="figure" role="figure" aria-label="GitLab logo sizing">
  <img class="figure-img p-a-5" src="/img/brand/core-logo-scalability.svg" alt="GitLab logo sizing" role="img" />
  <figcaption class="figure-caption">GitLab logo sizing</figcaption>
</figure>

## Placement

The GitLab logo and tanuki logomark are bold, eye-catching devices, so their placement requires thought and care. Alone, these assets can anchor text or expand to become the hero. They should not be overused or excessively repeated in a single asset.

Placement should primarily be left-aligned, either at the top or bottom corner, depending on which location balances the design and surrounding elements best. This placement allows the copy to be the main focus.

Centered placement should be reserved for times when the logo needs to be the main focus (i.e. video bumpers/end slates). In these cases, the logo is typically scaled larger.

Right-aligned corner placement (top or bottom) can be used in specific cases when left-aligned or centered doesn't work with the design layout or output medium. Right-aligned is only decided on a case-by-case basis and when there is valid reason for its usage.

## Logo variations

The full color logo is the default logo to be used, however, there are times that the one color variation of the logo and logomark can be used. The one color variation can be considered when…

- Restricted to a single-color use (i.e. print/merchandise applications)
- Additional logo brand awareness needed in combination with the the full color logo (i.e. large event booths)

<figure class="figure" role="figure" aria-label="Single color GitLab logo variations">
  <img class="figure-img p-a-5" src="/img/brand/core-logo-logo-variations.svg" alt="Single color logo use" role="img" />
  <figcaption class="figure-caption">Single color GitLab logo variations</figcaption>
</figure>

## Incorrect usage

<img class="d-block a-center gl-my-7" src="/img/brand/core-logo-incorrect-usage.svg" alt="GitLab logo incorrect usage" role="img" />

1. Do not apply a stroke to the logo
1. Do not recolor any part of the logo
1. Do not transform or alter the logo in any way
1. Do not rearrange the logo in any way
1. Do not apply drop shadow or effects to the logo
1. Do not rotate the logo in any way
1. Do not distort the logo in any way
1. Do not use the logo as a framing device for imagery
